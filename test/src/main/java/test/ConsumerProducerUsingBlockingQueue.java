package test;

import java.util.concurrent.*;

public class ConsumerProducerUsingBlockingQueue {

    static ArrayBlockingQueue<Integer> buffer = new ArrayBlockingQueue<>(2);

    public static void main(String[] args) {

        // Create a thread pool with two threads
        ExecutorService executor = Executors.newFixedThreadPool(4);
        executor.execute(new ProducerTask());
        executor.execute(new ConsumerTask());

        executor.execute(new ProducerTask());
        executor.execute(new ConsumerTask());

        executor.shutdown();

    }

}