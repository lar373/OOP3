package test;

import java.util.concurrent.*;

public class ParallelSum {
    public static void main(String[] args) {
        // Create a list
        final int N = 100000;
        double[] list = new double[N];
        for (int i = 0; i < list.length; i++) {
            list[i] = i; // to get double values
        }

        long startTime = System.currentTimeMillis();
        System.out.println("\nThe sum is " + sum(list));
        long endTime = System.currentTimeMillis();
        System.out.println("Number of processors is " + Runtime.getRuntime().availableProcessors());
        System.out.println("Time with " + (endTime - startTime) + " milliseconds");

    }

    public static double sum(double[] list) {
        RecursiveTask<Double> task = new SumTask(list, 0, list.length);
        ForkJoinPool pool = new ForkJoinPool();
        return pool.invoke(task);
    }

    public static class SumTask extends RecursiveTask<Double> {
        private static final long serialVersionUID = 1L;
        private final static int THRESHOLD = 10;
        private double[] list;
        private int low;
        private int high;

        public SumTask(double[] list, int low, int high) {
            this.list = list;
            this.low = low;
            this.high = high;
        }

        public Double compute() {
            if (low - high < THRESHOLD) {
                double sum = 0;
                for (int i = low; i < high; i++)
                    sum += list[i];
                return sum;
            } else {
                int mid = high - low;
                SumTask left = new SumTask(list, low, mid);
                SumTask right = new SumTask(list, mid, high);

                left.fork();
                right.fork();
                return (left.join().doubleValue() + right.join().doubleValue());
            }
        }
    }
}
