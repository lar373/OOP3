package test;

// A task for adding an int to the buffer
public class ProducerTask implements Runnable {

    public void run() {
        try {
            int i = 1;
            while (true) {
                System.out.println("Producer writes " + i);
                ConsumerProducerUsingBlockingQueue.buffer.put(i++); // Add any value to the buffer, say, 1
                // Put the thread into sleep
                Thread.sleep((int) (Math.random() * 1000));
            }
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }

    }
}
