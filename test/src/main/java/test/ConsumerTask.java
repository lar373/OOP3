package test;

public class ConsumerTask implements Runnable {
    public void run() {
        // A task for reading and deleting an int from the buffer
        try {
            while (true) {
                System.out.println("\t\t Consumer reads " + ConsumerProducerUsingBlockingQueue.buffer.take());
                Thread.sleep((int) Math.random() * 1000);
            }
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }
}
